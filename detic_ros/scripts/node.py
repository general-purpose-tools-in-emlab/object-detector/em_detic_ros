#!/usr/bin/env python3
from typing import Optional

import rospy
from node_config import NodeConfig
from rospy import Publisher, Subscriber
from sensor_msgs.msg import CompressedImage
from actionlib import SimpleActionServer
from wrapper import DeticWrapper

from detic_ros_msgs.msg import DeticBox, DeticBoxes, DeticGoal, DeticAction, DeticResult

import cv_bridge as _bridge

class DeticRosNode:
    detic_wrapper: DeticWrapper
    sub: Subscriber
    # some debug image publisher
    pub_debug_image: Optional[Publisher]
    pub_debug_segmentation_image: Optional[Publisher]

    # used when you set use_jsk_msgs = True
    pub_segimg: Optional[Publisher]

    # otherwise, the following publisher will be used
    pub_info: Optional[Publisher]

    def __init__(self, node_config: Optional[NodeConfig] = None):
        if node_config is None:
            node_config = NodeConfig.from_rosparam()

        self.detic_wrapper = DeticWrapper(node_config)

        if node_config.enable_pubsub:
            # As for large buff_size please see:
            # https://answers.ros.org/question/220502/image-subscriber-lag-despite-queue-1/?answer=220505?answer=220505#post-id-22050://answers.ros.org/question/220502/image-subscriber-lag-despite-queue-1/?answer=220505?answer=220505#post-id-220505
            self.sub = rospy.Subscriber('input_image', CompressedImage, self.callback_image, queue_size=1, buff_size=2**24)
            
            self.pub_segimg = rospy.Publisher('output_image', CompressedImage, queue_size=1)
            self.pub_bboxes = rospy.Publisher("output_bbox", DeticBoxes, queue_size=1)
            self._action_server = SimpleActionServer("/action/detection", DeticAction, self._action_callback, False)


        rospy.loginfo('initialized node')
        self._action_server.start()

    def callback_image(self, msg: CompressedImage):
        # Inference
        raw_result = self.detic_wrapper.infer(msg)

        # Publish main topics
        # if self.detic_wrapper.node_config.use_jsk_msgs:
        # assertion for mypy
        assert self.pub_segimg is not None
        # if self.pub_segimg.get_num_connections() > 0 and self.pub_bboxes.get_num_connections() > 0:
        seg_img_msg = raw_result.get_ros_debug_image()
        self.pub_segimg.publish(seg_img_msg)
        detic_bboxes_msg = raw_result.get_bouding_box()
        self.pub_bboxes.publish(detic_bboxes_msg)

        # Print debug info
        # if self.detic_wrapper.node_config.verbose:
        #     time_elapsed_total = (rospy.Time.now() - msg.header.stamp).to_sec()
        #     rospy.loginfo('total elapsed time in callback {}'.format(time_elapsed_total))

    def _action_callback(self, goal:DeticGoal):
        img_msg = goal.image
        raw_result = self.detic_wrapper.infer(img_msg)
        rospy.loginfo("called action callback")

        if not self._action_server.is_preempt_requested():
            result = DeticResult()
            result.boxes = raw_result.get_bouding_box()
            result.scores = raw_result.get_ros_score()
            self._action_server.set_succeeded(result)

        




if __name__ == '__main__':
    rospy.init_node('detic_node', anonymous=True)
    node = DeticRosNode()
    rospy.spin()