<!----------------------------------------------------------------------------------------------------------------------
#
#   Title
#
# --------------------------------------------------------------------------------------------------------------------->
# Detic-ROS
<!----------------------------------------------------------------------------------------------------------------------
#
#   Description
#
# --------------------------------------------------------------------------------------------------------------------->
- ROS wrapper repository for [Detic](https://github.com/facebookresearch/Detic/tree/46f8317a0688b4b94e85816b5839a1dbca26d1e0) (customized for Emergent System Lab.)
- Topic and Action communication is available  

![Input Image](docs/output.gif)  

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

<!----------------------------------------------------------------------------------------------------------------------
#
#   Table of Contents
#
# --------------------------------------------------------------------------------------------------------------------->
**Table of Contents:**
*   [Setup](#Setup)
*   [Launch](#launch)
*   [Files](#files)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Requirement
#
# --------------------------------------------------------------------------------------------------------------------->
## Requirements
* Required
    * Ubuntu: 20.04  
    * ROS: Noetic  
    * Python: 3.8
    * pytorch>=1.8

* Confirmed Condition
```
Ubuntu: 20.04LTS  
ROS: Noetic  
Python: 3.8.10  
torch: 1.9.1+cu111
torchvision: 0.10.1+cu111
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   Dependencies
#
# --------------------------------------------------------------------------------------------------------------------->
## Dependencies
Code wrapped from https://github.com/facebookresearch/Detic
Customized from https://github.com/HiroIshida/detic_ros

<!----------------------------------------------------------------------------------------------------------------------
#
#   Getting Started
#
# --------------------------------------------------------------------------------------------------------------------->
## Getting Started
```shell
git clone https://gitlab.com/general-purpose-tools-in-emlab/object-detector/segment-anything-model/fastsam_ros

```

```shell
cd em_detic_ros/bash
bash install_libraries_as_detic.bash
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   Weights
#
# --------------------------------------------------------------------------------------------------------------------->
## Weights
Get network weights by the shell. (`Detic_LCOCOI21k_CLIP_R5021k_640b32_4x_ft4x_max-size.pth`)  

```shell
cd em_detic_ros/detic_ros/models
bash download.bash
```

Other model is here.  
[MODEL_ZOO](https://gitlab.com/general-purpose-tools-in-emlab/object-detector/em_detic_ros/-/blob/main/detic_ros/Detic/docs/MODEL_ZOO.md?ref_type=heads)

<!----------------------------------------------------------------------------------------------------------------------
#
#   Example
#
# --------------------------------------------------------------------------------------------------------------------->
## Example
weight: `Detic_LCOCOI21k_CLIP_R5021k_640b32_4x_ft4x_max-size.pth`

```shell
roslaunch detic_ros node.launch
```

<!----------------------------------------------------------------------------------------------------------------------
#
#   ROS API
#
# --------------------------------------------------------------------------------------------------------------------->
## ROS API
### Detic Node

#### Subscribed Topics
* `/hsrb/head_rgbd_sensor/rgb/image_rect_color/compressed` (sensor_msgs/CompressedImage)

#### Published Topics
* `/detic/output/bboxes` (detic_ros_msgs/DeticBoxes)
* `/detic/output/image/compressed` (sensor_msgs/CompressedImage)

#### Action Subscribed Namespace
* `/detic/action/detection` (detic_ros_msgs/DeticAction)

#### Parameters
* `~verbose` (bool, default=True)
* `~model_type` (str, default="res50")
* `~enable_pubsub` (str, default="true")
* `~confidence_threshold` (str, default="0.5")
* `~input_image` (str, default="input")
* `~device` (str, default="auto")
* `~vocabulary` (str, default="objects365") other: 'lvis', 'openimages', 'objects365', 'coco', 'custom' (Adjust vocabulary in launch files if you use custom.)  
* `~custom_vocabulary` (str, default=" ")

<!----------------------------------------------------------------------------------------------------------------------
#
#   References
#
# --------------------------------------------------------------------------------------------------------------------->
## References
[Detic](https://github.com/facebookresearch/Detic/tree/46f8317a0688b4b94e85816b5839a1dbca26d1e0)  
[detic_ros](https://github.com/HiroIshida/detic_ros)
