#!/bin/bash

# Install detectron2
pip3 install detectron2 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu111/torch1.9/index.html

# Install other dependencies
pip3 install \
  opencv-python==4.5.5.62 \
  timm==0.5.4 \
  dataclasses \
  ftfy==6.0.3 \
  regex==2022.1.18 \
  fasttext==0.9.2 \
  scikit-learn==1.0.2 \
  numpy \
  lvis==0.5.3 \
  nltk==3.6.7 \
  bottleneck

# Install CLIP from GitHub
pip3 install git+https://github.com/openai/CLIP.git

# Install ROS packages
apt-get update && apt-get install -y --no-install-recommends \
  ros-noetic-jsk-common \
  ros-noetic-jsk-recognition \
&& rm -rf /var/lib/apt/lists/*